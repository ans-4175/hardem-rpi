#!/usr/bin/python

import urllib2,urllib
import json
import apiurl
import gardenerdevice

class Gardener(apiurl.ApiURL):
	def __init__(self, gardener_device, uid='sikaktus', api_url='http://192.168.30.77:3000'):
		self._gd = gardener_device
		super(Gardener, self).__init__(uid, api_url)
		
	def update(self):
		try:
			server_status = self.get_update()
			pump_command = bool(server_status['Pompa']['isOn'])
			lamp_on_state = bool(server_status['Lampu']['isOn'])
			
			if pump_command:
				self._gd.sprinkle(0.5)
				
			self._gd.set_lamp_on(lamp_on_state)
		except:
			pass
		
		moist = self._gd.read_humidity_sensor() * 1023.0
		print moist
		if moist > 400:
			# Sad T_T
			try:
				self.send_raw(20, 40, 60, 0)
			except:
				pass
			return 1
		else:
			# Happy ^^,
			try:
				self.send_raw(20, 40, 60, 2)
			except:
				pass
			return 0
	
		
