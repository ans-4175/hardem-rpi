#include <Firmata.h>

#define LIGHT_SENSOR 0
#define MOIST_SENSOR 1
#define TEMP_SENSOR 2
#define RELAY 12
#define trigPin 8
#define echoPin 7

/*==============================================================================
 * GLOBAL VARIABLES
 *============================================================================*/

/* analog inputs */
int analogInputsToReport = 0; // bitwise array to store pin reporting
int analogPin = 0; // counter for reading analog pins
/* timer variables */
unsigned long currentMillis;     // store the current value from millis()
unsigned long previousMillis;    // for comparison with currentMillis
/* pins configuration */
byte previousPIN[TOTAL_PORTS];  // PIN means PORT for input
byte previousPORT[TOTAL_PORTS]; 

/*==============================================================================
 * FUNCTIONS                                                                
 *============================================================================*/
void setPinModeCallback(byte pin, int mode) {
    if (IS_PIN_DIGITAL(pin)) {
        pinMode(PIN_TO_DIGITAL(pin), mode);
    }
}

void digitalWriteCallback(byte port, int value)
{
    byte i;
    byte currentPinValue, previousPinValue;

    if (port < TOTAL_PORTS && value != previousPORT[port]) {
        for(i=0; i<8; i++) {
            currentPinValue = (byte) value & (1 << i);
            previousPinValue = previousPORT[port] & (1 << i);
            if(currentPinValue != previousPinValue) {
                digitalWrite(i + (port*8), currentPinValue);
            }
        }
        previousPORT[port] = value;
    }
}

void setup() {
  // put your setup code here, to run once:
  Firmata.setFirmwareVersion(0, 2);
  Firmata.attach(DIGITAL_MESSAGE, digitalWriteCallback);
  Firmata.attach(SET_PIN_MODE, setPinModeCallback);

  Firmata.begin(57600);
  //Serial.begin(57600); 
  digitalWrite(RELAY,1);   //Turn OFF Relay 
  pinMode(RELAY, OUTPUT);
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
}

void loop() {
   while (Firmata.available()) {
        Firmata.processInput();
    }
    
    currentMillis = millis();
    if (currentMillis - previousMillis > 100) {  
        previousMillis += 100;        // Run this every 20ms

        Firmata.sendAnalog(0, readLight());  // Temperature sensor
        Firmata.sendAnalog(1, readMoist());  // Humidity sensor
        Firmata.sendAnalog(2, readTemp());  // Light sensor
        Firmata.sendAnalog(3, readSonar());  // Proximity sensor
    }
  

  //digitalWrite(RELAY,0);   //Turn ON Relay
  //digitalWrite(RELAY,1);   //Turn OFF Relay

}

float readLight(){
 return  analogRead(LIGHT_SENSOR);
}

float readMoist(){
  return analogRead(MOIST_SENSOR);
}

float readTemp(){
 int temp = analogRead(TEMP_SENSOR);
 return  temp * 0.48828125;  
}

long readSonar(){
  long duration;
  long distance = 0;
  
  digitalWrite(trigPin,LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  
  duration = pulseIn(echoPin, HIGH);
  distance = (long)(duration/2.0 / 29.1);
  
  return distance;
}


