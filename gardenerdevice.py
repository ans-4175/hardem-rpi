#
# HardEm
#
# (c) 2015 Labtek Indie
# All rights reserved.

import threading
from pyfirmata import Arduino, util

class GardenerDevice():
	def __init__(self, portname):
		self._board = None
		self._portname = portname
	
	def connect(self):
		self._board = Arduino(self._portname)
		self._it = util.Iterator(self._board)
		self._it.start()
		for i in range(0, 4):
			self._board.analog[i].enable_reporting()
		
	def disconnect(self):
		self._board.exit()
		self._board = None
	
	def read_temperature_sensor(self):
		return self._board.analog[0].read()
		
	def read_humidity_sensor(self):
		return self._board.analog[1].read()
		
	def read_light_sensor(self):
		return self._board.analog[2].read()
		
	def read_proximity_sensor(self):
		return self._board.analog[3].read()
		
	def set_pump_on(self, on):
		self._board.digital[12].write(not on)   # active-low
		
	def set_lamp_on(self, on):
		self._board.digital[1].write(on)
		
	def _stop_pump(self):
		self.set_pump_on(False)
	
	def sprinkle(self, seconds):
		self.set_pump_on(True)
		threading.Timer(seconds, self._stop_pump).start()
		

