#!/usr/bin/python

import urllib2,urllib
import json

class ApiURL(object):
	def __init__(self, uid='sikaktus', api_url='http://192.168.30.77:3000'):
		self._api_url = api_url
		self._uid = uid

	def send_raw(self,humidRaw,tempRaw,luxRaw,state):
		param = {}
		data = {}
		#isiin data raw dan compute lojik serta state
		data['humid'] = 0
		data['humidRaw'] = humidRaw
		data['temp'] = 0
		data['tempRaw'] = tempRaw
		data['lux'] = 0
		data['luxRaw'] = luxRaw
		data['state'] = state
		stringdata = json.dumps(data)
		#---
		param['data'] = stringdata
		url_values = urllib.urlencode(param)
		full_url = self._api_url + '/send/' + self._uid + '/?' + url_values
		response = urllib2.urlopen(full_url)
		
		return response
				
	def present_on(self):
		full_url = self._api_url + '/present/on/' + self._uid
		response = urllib2.urlopen(full_url)
		
		return response

	def present_off(self):
		full_url = self._api_url + '/present/off/' + self._uid
		response = urllib2.urlopen(full_url)
		
		return response
	
	def get_update(self):
		full_url = self._api_url + '/check/' + self._uid
		response = urllib2.urlopen(full_url)
		json_data = json.load(response)
		
		return json_data
		
"""apiurl = ApiURL()

apiurl.send_raw(900,22,900)
print apiurl.get_update()
apiurl.present_on()
print apiurl.get_update()
apiurl.present_off()
print apiurl.get_update()"""
