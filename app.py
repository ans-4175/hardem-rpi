#!/usr/bin/python
#
# HardEm
#
# (c) 2015 Labtek Indie
# All rights reserved.

import time
import lcddisplay
import gardenerdevice
import gardener

from PIL import Image
import Adafruit_Nokia_LCD as LCD

FACES_ASSETS = {
	'happy': 'assets/happy_lcd.png',
	'neutral': 'assets/neutral_lcd.png',
	'sad': 'assets/sad_lcd.png',
	'sleep': 'assets/sleep_lcd.png'
}

print 'HardEm -- Labtek Indie 2015'
print 'Initializing...'

# Initialization
disp = lcddisplay.LcdDisplay()
gardener_device = gardenerdevice.GardenerDevice('/dev/ttyUSB0')
gardener_device.connect()
gardener = gardener.Gardener(gardener_device, api_url='http://192.168.1.101:3000')

# Load assets
faces = {}
for asset_item in FACES_ASSETS.items():
	image = Image.open(asset_item[1])
	ratio = max(1.0*LCD.LCDWIDTH/image.size[0], 1.0*LCD.LCDHEIGHT/image.size[1])
 	image = image.resize((int(image.size[0]*ratio), int(image.size[1]*ratio)), Image.ANTIALIAS).convert('1')
	faces[asset_item[0]] = image

print 'Press CTRL-C to exit'

disp.set_backlight(True)
disp.load_and_display_image('logo.png')
time.sleep(2)

# exit()

sampling_delay = 0.2
try:
	while True:
		state = gardener.update()
		if state == 1:
			disp.display_image(faces['sad'])
		else:
			disp.display_image(faces['happy'])
		time.sleep(sampling_delay)
except KeyboardInterrupt:
	pass
		
gardener_device.disconnect()
disp.clear()
print "Goodbye cruel world..."



